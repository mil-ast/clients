export interface ClientStruct {
    id: number;
    name: string;
    needs: number;
    city_id: number;
    region_id?: number;
    district_id?: number;
    raiting: number;
    inn?: string;
    address?: string;
    business_area_id?: number;
    tenders?: string;
}

export class Client {
    id: number;
    name: string;
    needs: number;
    city_id: number;
    region_id?: number;
    district_id?: number;
    raiting: number;
    inn: string;
    address: string;
    business_area_id?: number;
    tenders?: string;

    constructor(data: ClientStruct) {
        this.id = data.id|0;
        this.update(data);
    }

    update(data: ClientStruct) {
        this.name = data.name||'';
        this.needs = data.needs||0;
        this.city_id = data.city_id||0;
        this.region_id = data.region_id||0;
        this.district_id = data.district_id||0;
        this.raiting = data.raiting||0;
        this.inn = data.inn||null;
        this.address = data.address||null;
        this.business_area_id = data.business_area_id||null;
        this.tenders = data.tenders||'N';
    }

    /**
     * получим состояние иконки потребностей
     */
    getStateIcon(pos: number = 0) {
        return ((this.needs >> pos) & 1) === 1;
    }

    /**
     * уровень рейтинга
     */
    getRaitingName() {
        switch (this.raiting) {
            case 1: case 2:
                return 'low';
            case 3: case 4:
                return 'low_plus';
            case 5: case 6:
                return 'medium';
            case 7: case 8:
                return 'hight_minus';
            case 9: case 10:
                return 'hight';
            default:
                return 'empty';
        }
    }
}