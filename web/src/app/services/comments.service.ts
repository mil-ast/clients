import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CommentsService {
    constructor(
		private http: HttpClient
    ) { }

    Get(id: number) {
        return this.http.get(`/api/client/${id}/comments`);
    }

    Create(client_id: number, comment: string) {
        const data = {
            comment : comment,
        };
        return this.http.post(`/api/client/${client_id}/comments`, data);
    }

    Delete(id: number) {
        return this.http.delete(`/api/client/comments/${id}`);
    }
}