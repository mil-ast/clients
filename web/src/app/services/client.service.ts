import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ClientService {
    private url = '/api/client/';
    private url_all = '/api/clients';
    private eventUpdate: Subject<any> = new Subject();

    constructor(
		private http: HttpClient
    ) { }

    EventOnUpdate(): Subject<any> {
        return this.eventUpdate;
    }

    Get(id: number) {
        return this.http.get(this.url.concat(id.toString()));
    }
    
    GetAll() {
        return this.http.get(this.url_all);
    }

    Create(data: any) {
        return this.http.post(this.url_all, data);
    }

    Update(id: number, data: any) {
        const subj = new Subject();

        const req = this.http.put(this.url.concat(id.toString()), data);
        req.subscribe((res: any) => {
            this.eventUpdate.next(res);
            subj.next(res);
        }, (err: Error) => {
            subj.error(err);
        }, () => {
            subj.complete();
        });

        return subj;
    }

    Delete(id: number) {
        return this.http.delete(this.url.concat(id.toString()));
    }
}
