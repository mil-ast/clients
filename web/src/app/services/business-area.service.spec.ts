import { TestBed, inject } from '@angular/core/testing';

import { BusinessAreaService } from './business-area.service';

describe('BusinessAreaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BusinessAreaService]
    });
  });

  it('should be created', inject([BusinessAreaService], (service: BusinessAreaService) => {
    expect(service).toBeTruthy();
  }));
});
