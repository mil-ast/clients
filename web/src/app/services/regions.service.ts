import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RegionsService {
  private url = '/api/regions/?district_id=';

  constructor(
  private http: HttpClient
  ) { }

  Get(district_id: number) {
      return this.http.get(this.url.concat(district_id.toString()));
  }
}
