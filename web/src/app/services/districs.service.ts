import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DistricsService {
    private url = '/api/districs';

    constructor(
		private http: HttpClient
    ) { }

    Get() {
        return this.http.get(this.url);
    }
}
