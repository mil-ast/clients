import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class CitiesService {
    private url = '/api/cities';
    cities: Map<number, any> = new Map();

	constructor(
		private http: HttpClient
    ) { }
    
    Sync() {
        const sbj = new Subject();

		this.http.get(this.url).subscribe((res: any[]) => {
            if (!Array.isArray(res)) {
                return;
            }

            for (let i = 0; i < res.length; i++) {
                this.Push(res[i].id, res[i]);
            }
        }, (err: Error) => {
            console.error(err);
        }, () => {
            sbj.next(this.Get());
            sbj.complete();
        });

        return sbj;
    }

    Push(id: number, data: any) {
        this.cities.set(id, data);
    }
    
    Get(region_id: number = 0): any[] {
        let list: any[] = [];
        this.cities.forEach((item: any, key: number) => {
            if (region_id !== 0){
                if (item.region_id !== region_id) {
                    return;
                }
            }

            list.push(item);
        });

        return list;
    }
    
    GetNameByID(id: number) {
        const item = this.cities.get(id);
        if (!item) {
            return '';
        } else {
            return item.name;
        }
    }
}
