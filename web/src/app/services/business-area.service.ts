import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class BusinessAreaService {
    private url: string = '/api/business_area';

    private list: Map<number, any> = new Map();

	constructor(
		private http: HttpClient
    ) { }

    Sync() {
        const subj = new Subject();

        const req = this.http.get(this.url);
        req.subscribe((res: any[]) => {
            const list = res||[];

            for (let i = 0; i < list.length; i++) {
                this.list.set(list[i].id, list[i]);
            }

            subj.next(this.GetAll());
        }, (err: Error) => {
            subj.error(err);
        }, () => {
            subj.complete();
        });

        return subj;
    }

    GetNameByID(id: number): string {
        return this.list.get(id)||'';
    }

    GetAll() {
        const responce: any = [];
        this.list.forEach((row: any) => {
            responce.push(row);
        });

        return responce;
    }
}
