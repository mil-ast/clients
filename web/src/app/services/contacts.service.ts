import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactsService {
    constructor(
		private http: HttpClient
    ) { }

    Get(client_id: number) {
        return this.http.get(`/api/client/${client_id}/contacts`);
    }

    Create(client_id: number, data: any) {
        return this.http.post(`/api/client/${client_id}/contacts`, data);
    }

    Update(id: number, data: any) {
        return this.http.put(`/api/clients/contacts/${id}`, data);
    }

    UpdateDate(id: number) {
        return this.http.put(`/api/clients/contacts/update_date/${id}`, {});
    }

    Delete(id: number) {
        return this.http.delete(`/api/clients/contacts/${id}`);
    }

    CreateContact(contact_id: number, data: any) {
        return this.http.post(`/api/clients/contacts/${contact_id}/contact`, data);
    }
    DeleteContact(id: number) {
        return this.http.delete(`/api/clients/contacts/contact/${id}`);
    }
}
