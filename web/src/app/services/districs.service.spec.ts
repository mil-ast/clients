import { TestBed, inject } from '@angular/core/testing';

import { DistricsService } from './districs.service';

describe('DistricsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DistricsService]
    });
  });

  it('should be created', inject([DistricsService], (service: DistricsService) => {
    expect(service).toBeTruthy();
  }));
});
