import { Component, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subject } from 'rxjs/Subject';
import { DialogUpdateContactComponent } from '../../dialogs/update-contact/update-contact.component';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.css']
})
export class ContactItemComponent {
    @Input() model: any;
    @Output() eventDelete: Subject<any> = new Subject();

    constructor(
        public dialog: MatDialog,
        private contactsService: ContactsService,
    ) { }

    ShowDialogUpdate() {
        let dialogRef = this.dialog.open(DialogUpdateContactComponent, {
            width: '800px',
            position: {
                top: '20px',
                left: '20px',
            },
            data: this.model
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }

            this.model.name = result.name;
            this.model.comment = result.comment;
        });

        return false;
    }

    /**
     * актуализация даты
     */
    ClickUpdateDate() {
        const req = this.contactsService.UpdateDate(this.model.id);
        req.subscribe((res: any) => {
            this.model.date_actual = res.date_actual;
        });
        return false;
    }

    Delete() {
        if (!confirm(`Удалить ${this.model.name}?`)) {
            return false;
        }

        const req = this.contactsService.Delete(this.model.id);
        req.subscribe(() => {
            this.eventDelete.next(this.model);
            this.eventDelete.complete();
        }, (err: Error) => {
            console.error(err);
            alert('Произошла ошибка');
        });

        return false;
    }
}
