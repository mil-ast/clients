import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Client } from '../../../classes/client.class';
import { CommentsService } from '../../../services/comments.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
    @Input() model: Client;
    @Input() eventCreate: Subject<any>;
    comments: any[] = [];

    constructor(
        private commentsService: CommentsService,
    ) { }

    ngOnInit() {
        const req = this.commentsService.Get(this.model.id);
        req.subscribe((res: any[]) => {
            this.comments = res||[];
        });

        this.eventCreate.subscribe((message: any) => {
            this.comments.push(message);
        });
    }

    Delete(c: any) {
        if (!confirm('Удалить комментарий?')) {
            return false;
        }

        const req = this.commentsService.Delete(c.id);
        req.subscribe(() => {
            const index = this.comments.indexOf(c);
            if (index !== -1) {
                this.comments.splice(index, 1);
            }
        });

        return false;
    }
}
