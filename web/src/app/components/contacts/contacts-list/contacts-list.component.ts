import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Client } from '../../../classes/client.class';
import { ContactsService } from '../../../services/contacts.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {
    @Input() model: Client;
    contacts: any[] = [];
    @Input() eventCreate: Subject<any>;
    
    constructor(
        private contactsService: ContactsService,
    ) {}

    ngOnInit() {
        const req = this.contactsService.Get(this.model.id);
        req.subscribe((res: any[]) => {
            this.contacts = res||[];
        });

        this.eventCreate.subscribe((message: any) => {
            this.contacts.push(message);
        });
    }

    ngOnDestroy() {
        this.eventCreate.unsubscribe();
    }

    OnDeleteContact(event) {
        const index: number = this.contacts.indexOf(event);
        if (index !== -1) {
            this.contacts.splice(index, 1);
        }
    }
}
