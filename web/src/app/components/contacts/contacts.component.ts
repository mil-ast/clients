import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog } from '@angular/material';
import { ClientService } from '../../services/client.service';
import { CitiesService } from '../../services/cities.service';
import { BusinessAreaService } from '../../services/business-area.service';
import { Client, ClientStruct } from '../../classes/client.class';
import { DialogUpdateClientComponent } from '../dialogs/update-client/update-client.component';
import { DialogCreateContactComponent } from '../dialogs/create-contact/create-contact.component';
import { DialogCreateCommentComponent } from '../dialogs/create-comment/create-comment.component';
import { fromEvent, Subject } from 'rxjs';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css', '../../css/needs.icons.css']
})
export class ContactsComponent implements OnInit {
    client: Client;
    eventCreateContact: Subject<any> = new Subject();
    eventCreateComment: Subject<any> = new Subject();

    constructor(
        private router: Router,
        private activated_route: ActivatedRoute,
        public dialog: MatDialog,
        private clientService: ClientService,
        private citiesService: CitiesService,
        private businessAreaService: BusinessAreaService,
    ) {
        this.client = new Client({
            id : 0,
            name: '',
            needs: 0,
            city_id: 0,
            raiting:  0,
        });
    }

    ngOnInit() {
        const source = fromEvent(document.body, 'keyup').subscribe((e: KeyboardEvent) => {
            if (e.keyCode === 27) {
                source.unsubscribe();
                this.router.navigate(['/']);
            }
        });

        this.activated_route.params.subscribe((params: any) => {
            const id: number = params.id|0;

            const req = this.clientService.Get(id);
            req.subscribe((res: ClientStruct) => {
                this.client = new Client(res);
            });
        });
    }

    GetCityName() {
        return this.citiesService.GetNameByID(this.client.city_id|0);
    }

    /**
     * диалог редактирования клиента
     */
    ShowDialogUpdateClient() {
        let dialogRef = this.dialog.open(DialogUpdateClientComponent, {
            width: '700px',
            position: {
                top: '20px',
                left: '20px',
            },
            data: this.client
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }
        });

        return false;
    }

    /**
     * диалог добавления контакта
     */
    ShowDialogCreateContact() {
        let dialogRef = this.dialog.open(DialogCreateContactComponent, {
            width: '400px',
            position: {
                top: '20px',
                left: '20px',
            },
            data: this.client
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }

            this.eventCreateContact.next(result);
        });

        return false;
    }

    /**
     * форма добавления комментария
     */
    ShowDialogCreateComment() {
        const dialogRef = this.dialog.open(DialogCreateCommentComponent, {
            width: '400px',
            position: {
                top: '20px',
                right: '20px',
            },
            data: this.client
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }

            this.eventCreateComment.next(result);
        });

        return false;
    }

    GetBusinessAreaName() {
        if (this.client.business_area_id === null) {
            return '';
        }

        const area: any = this.businessAreaService.GetNameByID(this.client.business_area_id);
        if (!area) {
            return '';
        }

        return area.name;
    }
}
