import {Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ContactsService } from '../../../services/contacts.service';

@Component({
    selector: 'app-update-contact',
    templateUrl: './update-contact.component.html',
    styleUrls: [
        '../../../css/dialogs.css',
        './update-contact.component.css'
    ]
})
export class DialogUpdateContactComponent implements OnInit {
    reactiveForm: FormGroup;
    reactiveFormAddContact: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DialogUpdateContactComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
        private contactsService: ContactsService,
    ) {
		this.reactiveForm = this.fb.group({
			name: ['', [Validators.required, Validators.maxLength(60)]],
			comment: ['', Validators.maxLength(255)],
        });

        this.reactiveFormAddContact = this.fb.group({
			type: ['phone', Validators.required],
			value: ['', [Validators.required, Validators.maxLength(255)]],
        });
    }

    ngOnInit() {
        this.reactiveForm.patchValue({
            name : this.data.name,
            comment : this.data.comment,
        });
    }

    Submit() {
        const data = {
            name : this.reactiveForm.value.name,
            comment : this.reactiveForm.value.comment,
        };

        const req = this.contactsService.Update(this.data.id, data);
        req.subscribe((res: any) => {
            this.dialogRef.close(res)
        });

        return false;
    }

    SubmitAddContact() {
        const data = this.reactiveFormAddContact.value;
        const req = this.contactsService.CreateContact(this.data.id, data);
        req.subscribe((res) => {
            this.data.values.push(res);
        });

        this.reactiveFormAddContact.patchValue({
            value : ''
        });
        return false;
    }

    DeleteContact(v: any) {
        if (!confirm(`Удалить ${v.value}?`)) {
            return false;
        }
        
        const req = this.contactsService.DeleteContact(v.id);
        req.subscribe(() => {
            const index: number = this.data.values.indexOf(v);
            if (index !== -1) {
                this.data.values.splice(index, 1);
            }
        });
    }
}
