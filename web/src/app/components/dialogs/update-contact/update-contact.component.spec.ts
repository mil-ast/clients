import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogUpdateContactComponent } from './update-contact.component';

describe('DialogUpdateContactComponent', () => {
  let component: DialogUpdateContactComponent;
  let fixture: ComponentFixture<DialogUpdateContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogUpdateContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUpdateContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
