import {Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { CommentsService } from '../../../services/comments.service';
import { Client } from '../../../classes/client.class';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: [
    '../../../css/dialogs.css',
    './create-comment.component.css'
]
})
export class DialogCreateCommentComponent {
    reactiveForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DialogCreateCommentComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Client,
        private fb: FormBuilder,
        private commentsService: CommentsService,
    ) {
		this.reactiveForm = this.fb.group({
			comment: ['', Validators.required],
        });
    }

    Submit() {
        const req = this.commentsService.Create(this.data.id, this.reactiveForm.value.comment);
        req.subscribe((res: any) => {
            this.dialogRef.close(res)
        }, (err: Error) => {
            console.error(err);
            alert('Произошла ошибка');
        });
        return false;
    }

}
