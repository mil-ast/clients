import {Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ContactsService } from '../../../services/contacts.service';
import { Client } from '../../../classes/client.class';
@Component({
    selector: 'app-create-contact',
    templateUrl: './create-contact.component.html',
    styleUrls: [
        '../../../css/dialogs.css',
        './create-contact.component.css'
    ]
})
export class DialogCreateContactComponent {
    reactiveForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<DialogCreateContactComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Client,
        private fb: FormBuilder,
        private contactsService: ContactsService,
    ) {
		this.reactiveForm = this.fb.group({
			name: ['', [Validators.required, Validators.maxLength(60)]],
			comment: ['', Validators.maxLength(255)],
        });
    }

    Submit() {
        const data = {
            name : this.reactiveForm.value.name,
            comment : this.reactiveForm.value.comment,
        };

        const req = this.contactsService.Create(this.data.id, data);
        req.subscribe((res: any) => {
            this.dialogRef.close(res)
        });

        return false;
    }

}
