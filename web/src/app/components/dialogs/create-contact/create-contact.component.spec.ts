import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCreateContactComponent } from './create-contact.component';

describe('DialogCreateContactComponent', () => {
  let component: DialogCreateContactComponent;
  let fixture: ComponentFixture<DialogCreateContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCreateContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCreateContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
