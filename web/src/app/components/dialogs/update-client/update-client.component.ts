import {Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { CitiesService } from '../../../services/cities.service';
import { ClientService } from '../../../services/client.service';
import { BusinessAreaService } from '../../../services/business-area.service';
import { Client, ClientStruct } from '../../../classes/client.class';

@Component({
    selector: 'app-update-client',
    templateUrl: './update-client.component.html',
    styleUrls: [
        '../../../css/dialogs.css',
        '../../../css/dialogs_create_update_clients.css',
        './update-client.component.css'
    ]
})
export class DialogUpdateClientComponent implements OnInit {
    reactiveForm: FormGroup;
    cities: any[] = [];
    businessArea: any[] = [];
    filteredOptions: Observable<string[]>;

    constructor(
        public dialogRef: MatDialogRef<DialogUpdateClientComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Client,
        private fb: FormBuilder,
        private citiesService: CitiesService,
        private clientService: ClientService,
        private businessAreaService: BusinessAreaService,
    ) {
        const city_name = this.citiesService.GetNameByID(data.city_id|0)||'';

		this.reactiveForm = this.fb.group({
			name: [data.name, [Validators.required, Validators.maxLength(0xFFFF)]],
            city_id: [data.city_id, Validators.min(0)],
            city_name : city_name,
            raiting : [data.raiting, [Validators.min(0), Validators.max(10)]],
            inn : data.inn,
            address : data.address,
            business_area_id : data.business_area_id,
            tenders : data.tenders === 'Y' ? true : false,
            needs : data.needs
        });
    }

    ngOnInit() {
        this.filteredOptions = this.reactiveForm.valueChanges.pipe(
          startWith(null),
          map(value => this._filter(value))
        );

        this.cities = this.citiesService.Get();
        this.businessArea = this.businessAreaService.GetAll();

        // запросим дополнительные поля клиента, которых нет в основном списке
        const req = this.clientService.Get(this.data.id);
        req.subscribe((res: ClientStruct) => {
            this.reactiveForm.patchValue({
                address : res.address,
            });
        });
    }

    private _filter(item: any): any[] {
        if (item === null || item.city_name === '' || item.city_name === null) {
            return [];
        }

        const filterValue = item.city_name.toLowerCase();
        return this.cities.filter((option) => {
            return option.name.toLowerCase().indexOf(filterValue) === 0;
        });
    }


    /**
     * изменение потребностей
     */
    ChangeNeeds(el) {
        const bytes = 1 << (el.target.value|0);

        if (el.target.checked) {
            this.reactiveForm.value.needs |= bytes; // включаем бит
        } else {
            this.reactiveForm.value.needs = this.reactiveForm.value.needs & ~bytes; // отключаем бит
        }
    }

    Submit() {
        try {
            let city_id = null;
            if (this.reactiveForm.value.city_name !== '') {
                for (let i = 0; i < this.cities.length; i++) {
                    if (this.cities[i].name === this.reactiveForm.value.city_name) {
                        city_id = this.cities[i].id;
                        break;
                    }
                }
            }

            const values = {
                name : this.reactiveForm.value.name,
                city_id : city_id,
                needs : this.reactiveForm.value.needs,
                raiting : this.reactiveForm.value.raiting,
                inn : this.reactiveForm.value.inn,
                address : this.reactiveForm.value.address,
                tenders : this.reactiveForm.value.tenders ? 'Y' : 'N',
                business_area_id : this.reactiveForm.value.business_area_id,
            };

            const req = this.clientService.Update(this.data.id, values);
            req.subscribe((res: ClientStruct) => {
                this.data.update(res);
                
                this.dialogRef.close(this.data)
            });
            return false;
        } catch(err) {
            console.error(err);
            alert('Error');
        }
    }
}
