import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DialogCreateClientComponent } from '../dialogs/create-client/create-client.component';
import { ClientService } from '../../services/client.service';
import { CitiesService } from '../../services/cities.service';
import { DistricsService } from '../../services/districs.service';
import { RegionsService } from '../../services/regions.service';
import { BusinessAreaService } from '../../services/business-area.service';
import { Client, ClientStruct } from '../../classes/client.class';
import { forkJoin } from 'rxjs';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
    public clients: Client[] = [];
    public cities: any[] = [];
    public regions: any[] = [];
    public districs: any[] = [];
    public districts: any[] = [];

    filterForm: FormGroup;

    constructor(
        public dialog: MatDialog,
        private clientsService: ClientService,
        private citiesService: CitiesService,
        private districsService: DistricsService,
        private regionsService: RegionsService,
        private businessAreaService: BusinessAreaService,
    ) {
        this.filterForm = new FormGroup({
            name : new FormControl(''),
            city_id : new FormControl({value : 0, disabled : true}),
            region_id : new FormControl({value : 0, disabled : true}),
            district_id : new FormControl(0),
            tenders : new FormControl(null),
            needs : new FormControl(null),
        });

        const event = this.clientsService.EventOnUpdate();
        event.subscribe(this.onClientUpdate.bind(this));
    }

    ngOnInit() {
        forkJoin(
            this.clientsService.GetAll(),
            this.districsService.Get(),
            this.citiesService.Sync(),
            this.businessAreaService.Sync(),
        ).subscribe((res: any[]) => {
            const clients = res[0];
            this.districs = res[1]||[];

            this.clients = clients.map((item: ClientStruct) => {
                return new Client(item);
            });
        })
    }

    ShowDialogCreateClient() {
        let dialogRef = this.dialog.open(DialogCreateClientComponent, {
            width: '700px',
            position: {
                top: '20px',
                right: '20px',
            },
            data: {}
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }

            this.clients.unshift(new Client(result));
        });

        return false;
    }

    OnDeleteClient(event) {
        const index: number = this.clients.indexOf(event);
        if (index !== -1) {
            this.clients.splice(index, 1);
        }
    }

    /**
     * изменение в фильтре ФО
     */
    ChangeDistrict(event) {
        this.cities = [];
        this.regions = [];
        this.filterForm.controls.city_id.disable();

        if (event.value === 0) {
            this.filterForm.patchValue({
                region_id : 0,
                city_id : 0,
            });

            this.filterForm.controls.region_id.disable();

            this.regions = [];
            return;
        }

        const req = this.regionsService.Get(event.value);
        req.subscribe((res: any[]) => {
            this.regions = res||[];

            this.filterForm.patchValue({
                region_id : 0,
            });
            this.filterForm.controls.region_id.enable();
        });
    }

    /**
     * изменение в фильтре региона
     */
    ChangeRegion(event) {
        this.cities = [];

        if (event.value === 0) {
            this.filterForm.patchValue({
                city_id : 0,
            });

            this.filterForm.controls.city_id.disable();
            return;
        }

        this.cities = this.citiesService.Get(event.value);

        this.filterForm.patchValue({
            city_id : 0,
        });
        this.filterForm.controls.city_id.enable();
    }

    /**
     * для скачивания, получение id текущих клиентов
     */
    Download(event) {
        let need_number: number = 0x00;
        if (this.filterForm.controls.needs.value !== null) {
            for (let i = 0; i < this.filterForm.controls.needs.value.length; i++) {
                need_number |= (0x01 << this.filterForm.controls.needs.value[i]);
            }
        }

        const ids = this.clients.filter((item: Client) => {
            if (this.filterForm.controls.name.value !== '') {
                if (item.name.toLowerCase().indexOf(name) === -1) {
                    return false;
                }
            }

            if (this.filterForm.controls.city_id.value !== 0) {
                if (item.city_id !== this.filterForm.controls.city_id.value) {
                    return false;
                }
            } else if (this.filterForm.controls.region_id.value !== 0) {
                if (item.region_id !== this.filterForm.controls.region_id.value) {
                    return false;
                }
            } else if (this.filterForm.controls.district_id.value !== 0) {
                if (item.district_id !== this.filterForm.controls.district_id.value) {
                    return false;
                }
            }

            if (this.filterForm.controls.tenders.value !== null) {
                if (item.tenders !== this.filterForm.controls.tenders.value) {
                    return false;
                }
            }

            if (this.filterForm.controls.needs.value !== null) {
                const compare_byte: number = item.needs & need_number;
                if (compare_byte !== need_number) {
                    return false;
                }
            }

            return true;
        }).map((item: Client) => {
            return item.id;
        });

        if (ids.length === 0) {
            return false;
        }
        
        location.href = '/api/download/?ids='.concat(ids.join(','));

        return false;
    }

    private onClientUpdate(data: ClientStruct): void {
        const id: number = data.id|0;
        if (id === 0) {
            return;
        }

        for (let i = 0; i < this.clients.length; i++) {
            if (this.clients[i].id === id) {
                this.clients[i].update(data);
                break;
            }
        }
    }
}
