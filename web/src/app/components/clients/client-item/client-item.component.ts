import { Component, Input, Output } from '@angular/core';
import { DialogUpdateClientComponent } from '../../dialogs/update-client/update-client.component';
import { MatDialog } from '@angular/material';
import { CitiesService } from '../../../services/cities.service';
import { ClientService } from '../../../services/client.service';
import { BusinessAreaService } from '../../../services/business-area.service';
import { Client } from '../../../classes/client.class';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-client-item',
  templateUrl: './client-item.component.html',
  styleUrls: ['./client-item.component.css', '../../../css/needs.icons.css']
})
export class ClientItemComponent {
    @Input() model: Client;
    @Output() eventDelete: Subject<Client> = new Subject();
	
	constructor(
        public dialog: MatDialog,
		private citiesSerice: CitiesService,
		private clientsService: ClientService,
		private businessAreaService: BusinessAreaService,
	) { }

    ngOnDestroy() {
        this.eventDelete.complete();
    }
    
    GetCityName() {
        if (this.model.city_id === 0) {
            return '';
        }
        return this.citiesSerice.GetNameByID(this.model.city_id);
    }
    
    GetBusinessAreaName() {
        if (this.model.business_area_id === null) {
            return '';
        }

        const area: any = this.businessAreaService.GetNameByID(this.model.business_area_id);
        if (!area) {
            return '';
        }

        return area.name;
    }

    /**
     * диалог редактирования клиента
     */
    ShowDialogUpdateClient() {
        let dialogRef = this.dialog.open(DialogUpdateClientComponent, {
            width: '700px',
            position: {
                top: '20px',
                right: '20px',
            },
            data: this.model
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (!result) {
                return;
            }
        });

        return false;
    }

    Delete() {
        if (!confirm(`Удалить клиента ${this.model.name}?`)) {
            return false;
        }

        const req = this.clientsService.Delete(this.model.id);
        req.subscribe(() => {
            this.eventDelete.next(this.model);
        }, (err: Error) => {
            console.error(err);
        });

        return false;
    }
}
