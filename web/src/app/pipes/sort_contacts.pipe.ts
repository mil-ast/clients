import { Pipe, PipeTransform } from '@angular/core';

interface contactStruct {
    type: string;
}

@Pipe({
    name: 'contactSort',
    pure: false
})

export class SortContactsPipe implements PipeTransform {
    transform(array: contactStruct[]) {
        if (!Array.isArray(array)) {
            return [];
        }

		return array.sort((left: contactStruct, right: contactStruct) => {
            if (left.type === 'phone' && right.type === 'email') {
                return -1;
            } else if (left.type === 'email' && right.type === 'phone') {
                return 1;
            }
            return 0;
        });
    }
}