import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(array: any[], params: any) {
        if (!Array.isArray(array)) {
            return [];
        }

        const name: string = params.name.value.toLowerCase();
        const city_id: number = params.city_id.value|0;
        const region_id: number = params.region_id.value|0;
        const district_id: number = params.district_id.value|0;
        const needs: number[] = params.needs.value;
        let tenders: string = null;
        if (params.tenders.value !== null) {
            tenders = params.tenders.value ? 'Y' : 'N';
        }

		if (name === '' && district_id === 0 && region_id === 0 && city_id === 0 && tenders === null && needs === null) {
			return array;
        }

        let need_number: number = 0x00;
        if (needs !== null) {
            for (let i = 0; i < needs.length; i++) {
                need_number |= (0x01 << needs[i]);
            }
        }

        return array.filter((item: any) => { 
            if (name !== '') {
                if (item.name.toLowerCase().indexOf(name) === -1) {
                    return false;
                }
            }

            if (city_id !== 0) {
                if (item.city_id !== city_id) {
                    return false;
                }
            } else if (region_id !== 0) {
                if (item.region_id !== region_id) {
                    return false;
                }
            } else if (district_id !== 0) {
                if (item.district_id !== district_id) {
                    return false;
                }
            }

            if (tenders !== null) {
                if (item.tenders !== tenders) {
                    return false;
                }
            }

            if (needs !== null) {
                const compare_byte: number = item.needs & need_number;
                if (compare_byte !== need_number) {
                    return false;
                }
            }

            return true;
        });
    }
}

