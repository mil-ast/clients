import { Pipe, PipeTransform } from '@angular/core';
import { Client } from '../classes/client.class';
@Pipe({
    name: 'clientsSort',
    pure: false
})

export class SortClientsPipe implements PipeTransform {
    transform(array: Client[]) {
        if (!Array.isArray(array)) {
            return [];
        }

		return array.sort((left: Client, right: Client) => {
            if (left.id > right.id) {
                return -1;
            } else if (left.id < right.id) {
                return 1;
            }

            return 0;
        });
    }
}