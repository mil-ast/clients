import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
// модули
import { AppRoutingModule } from './app.routing';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ClientsComponent } from './components/clients/clients.component';
import {
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatCheckboxModule,
} from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ClientItemComponent } from './components/clients/client-item/client-item.component';
import { DialogCreateClientComponent } from './components/dialogs/create-client/create-client.component';
import { DialogUpdateClientComponent } from './components/dialogs/update-client/update-client.component';
import { DialogCreateContactComponent } from './components/dialogs/create-contact/create-contact.component';
import { DialogUpdateContactComponent } from './components/dialogs/update-contact/update-contact.component';
import { DialogCreateCommentComponent } from './components/dialogs/create-comment/create-comment.component';

// сервисы
import { ClientService } from './services/client.service';
import { CitiesService } from './services/cities.service';
import { DistricsService } from './services/districs.service';
import { RegionsService } from './services/regions.service';
import { ContactsService } from './services/contacts.service';
import { CommentsService } from './services/comments.service';
import { BusinessAreaService } from './services/business-area.service';
import { ContactsListComponent } from './components/contacts/contacts-list/contacts-list.component';
import { ContactItemComponent } from './components/contacts/contact-item/contact-item.component';
import { CommentsComponent } from './components/contacts/comments/comments.component';
// пайпы
import { FilterPipe } from './pipes/filter.pipe';
import { SortContactsPipe } from './pipes/sort_contacts.pipe';
import { SortClientsPipe } from './pipes/sort_clients.pipe';


@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    ClientsComponent,
    ClientItemComponent,
    ContactsListComponent,
    ContactItemComponent,
    CommentsComponent,
    DialogCreateClientComponent,
    DialogUpdateClientComponent,
    DialogCreateContactComponent,
    DialogUpdateContactComponent,
    DialogCreateCommentComponent,
    FilterPipe,
    SortContactsPipe,
    SortClientsPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatCheckboxModule,
  ],
  providers: [
    ClientService,
    CitiesService,
    DistricsService,
    RegionsService,
    ContactsService,
    CommentsService,
    BusinessAreaService,
  ],
  entryComponents : [
    DialogCreateClientComponent,
    DialogUpdateClientComponent,
    DialogCreateContactComponent,
    DialogUpdateContactComponent,
    DialogCreateCommentComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
