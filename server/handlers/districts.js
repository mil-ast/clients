
const conn = require('../db');

app.get('/api/districs', function (req, res) {
    const query_sql = 'SELECT * FROM `geo_district`';
    conn.query(query_sql, (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        res.send(result);
    });
});
