const conn = require('../db');

app.get('/api/client/:id/comments', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'SELECT * FROM `comments` WHERE `client_id`=?';
    conn.query(query_sql, [id], (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        res.send(result);
    });
});
/**
 * создание комментария
 */
app.post('/api/client/:id/comments', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'INSERT INTO `comments` SET ?';
    const values = {
        client_id : id,
        time : new Date(),
        comment : req.body.comment,
    };

    conn.query(query_sql, values, (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        const responce = {
            id : result.insertId,
            time : values.time,
            comment : req.body.comment,
        };

        res.status(201).send(responce);
    });
});
/**
 * удаление комментария
 */
app.delete('/api/client/comments/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'DELETE FROM `comments` WHERE `id`=?';

    conn.query(query_sql, [id], (err) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        res.send(204);
    });
});
