const conn = require('../db');

app.get('/api/clients', function (req, res) {
    const query_sql = 'SELECT `clients`.`id`,`clients`.`name`,`clients`.`needs`,`clients`.`city_id`,`clients`.`raiting`,`clients`.`inn`,`clients`.`business_area_id`,`clients`.`tenders`,`geo_regions`.`id` AS `region_id`,`geo_regions`.`district_id` '+
        'FROM `clients` '+
        'LEFT JOIN `geo_city` ON `geo_city`.`id`=`clients`.`city_id` '+
        'LEFT JOIN `geo_regions` ON `geo_regions`.`id`=`geo_city`.`region_id`';
    
    conn.query(query_sql, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.send(500);
            return;
        }

        res.send(result);
    });
});

/**
 * создание клиента
 */
app.post('/api/clients', function (req, res) {
    const query_sql = 'INSERT INTO `clients` SET ?';

    let query_data = {
        name : req.body.name,
        needs : req.body.needs,
        city_id : req.body.city_id,
        raiting : req.body.raiting,
        inn : req.body.inn,
        address : req.body.address,
        tenders : req.body.tenders,
        business_area_id : req.body.business_area_id,
    };

    conn.query(query_sql, query_data, (err, result) => {
        if (err !== null) {
            console.log(err);
            res.send(500);
            return;
        }

        res.status(201).send({
            id : result.insertId,
            name : req.body.name,
            needs : req.body.needs,
            city_id : req.body.city_id,
            raiting : req.body.raiting,
            inn : req.body.inn,
            tenders : req.body.tenders,
            business_area_id : req.body.business_area_id,
        });
    });
});

/**
 * изменение клиента
 */
app.put('/api/client/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return;
    }

    const query_sql = 'UPDATE `clients` SET `name`=?,`needs`=?,`city_id`=?,`raiting`=?,`inn`=?,`address`=?,`business_area_id`=?,`tenders`=? WHERE `id`=?';
    const query_data = [
        req.body.name,
        req.body.needs,
        req.body.city_id,
        req.body.raiting,
        req.body.inn,
        req.body.address,
        req.body.business_area_id,
        req.body.tenders,
        id,
    ];

    conn.query(query_sql, query_data, (err) => {
        if (err !== null) {
            console.log(err);
            res.send(500);
            return;
        }

        const responce = {
            id : id,
            name : req.body.name,
            needs : req.body.needs,
            city_id : req.body.city_id,
            raiting : req.body.raiting,
            inn : req.body.inn,
            tenders : req.body.tenders,
            address : req.body.address,
            business_area_id : req.body.business_area_id,
        };

        res.status(202).send(responce);
    });
});

/**
 * удалить клиента
 */
app.delete('/api/client/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return;
    }
    
    const query_sql = 'DELETE FROM `clients` WHERE `id`=?';
    conn.query(query_sql, [id], (err) => {
        if (err !== null) {
            console.error(err);
            res.send(500);
            return;
        }

        res.sendStatus(204);
    });
});


function getGeo(city_id) {
    let geo_promise;

    if (city_id) {
        geo_promise = new Promise((resolve, reject) => {
            const query_sql = 'SELECT `geo_city`.`region_id`,`geo_regions`.`district_id` '+
                'FROM `geo_city` '+
                'INNER JOIN `geo_regions` ON `geo_regions`.`id`=`geo_city`.`region_id` '+
                'WHERE `geo_city`.`id`=?';
            
            conn.query(query_sql, [city_id], (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
                const row = result[0];

                resolve({
                    region_id : row.region_id|0,
                    district_id : row.district_id|0,
                });
            });
        });
    } else {
        geo_promise = new Promise((resolve) => {
            resolve({
                region_id : null,
                district_id : null,
            });
        });
    }

    return geo_promise;
}