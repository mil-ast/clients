
const conn = require('../db');

app.get('/api/download/', function (req, res) {
    const conn = require('../db');
    const Excel = require('exceljs');

    const workbook = new Excel.Workbook();
    const sheet = workbook.addWorksheet('Sheet', {
        pageSetup: {
            paperSize: 9,
            orientation:'portrait',
            scale : 78,
            fitToPage: true,
            fitToHeight: 1,
            fitToWidth: 1,
            margins : {
                left: 0.3, right: 0.3,
                top: 0.1, bottom: 0.30,
                header: 0.2, footer: 0.2
            }
        },
        properties : {
            defaultRowHeight : 15
        }
    });

    // первая колонка
    for (let i = 1; i < 23; i++) {
        const col = sheet.getColumn(i);
    
        switch (i) {
        case 1:
            col.width = 46;
        break;
        case 2: case 3:
            col.width = 17;
        break;
        case 4:
            col.width = 46;
        break;
        case 17:
            col.width = 20;
        break;
        case 18: case 19: case 20: case 21: case 22: case 23:
            col.width = 18;
        break;
        }
    }

    // заголовки
    let row = sheet.getRow(1);
    let cell = row.getCell(1);
    cell.value = 'Название';
    cell = row.getCell(2);
    cell.value = 'ИНН';
    cell = row.getCell(3);
    cell.value = 'Город';
    cell = row.getCell(4);
    cell.value = 'Адрес';
    cell = row.getCell(5);
    cell.value = 'Монтаж';
    cell = row.getCell(6);
    cell.value = 'Комутация';
    cell = row.getCell(7);
    cell.value = 'АСУ ТП';
    cell = row.getCell(8);
    cell.value = 'Питание';
    cell = row.getCell(9);
    cell.value = 'Частотники';
    cell = row.getCell(10);
    cell.value = 'Шкафы';
    cell = row.getCell(11);
    cell.value = 'Модемы';
    cell = row.getCell(12);
    cell.value = 'Кабель';
    cell = row.getCell(13);
    cell.value = 'Двигатели';
    cell = row.getCell(14);
    cell.value = 'Измерения';
    cell = row.getCell(15);
    cell.value = 'Пневматика';

    const query_ids = req.query.ids||null;
    if (query_ids === null) {
        res.send(400);
        return;
    }
    const ids = query_ids.split(',');
    const sql_query_tpl = ',?'.repeat(ids.length).substr(1);

    Promise.all([
        // получаем всех клиентов
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `clients`.`id`,`clients`.`name`,`clients`.`needs`,`clients`.`inn`,`clients`.`address`,`geo_city`.`name` AS `city_name`,`contacts`.`id` AS `contact_id`,`contacts`.`name` AS `contact_name` '+
                'FROM `clients` '+
                'LEFT JOIN `geo_city` ON `geo_city`.`id`=`clients`.`city_id` '+
                'LEFT JOIN `contacts` ON `contacts`.`client_id`=`clients`.`id` '+
                'WHERE `clients`.`id` IN ('.concat(sql_query_tpl, ') ') +
                'ORDER BY `clients`.`id` DESC';

            conn.query(query_sql, ids, (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
        
                resolve(result);
            });
        }),
        // получаем сами контакты
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `contacts_values`.`contact_id`,`contacts_values`.`value` '+
                'FROM `clients` '+
                'INNER JOIN `contacts` ON `contacts`.`client_id`=`clients`.`id` '+
                'INNER JOIN `contacts_values` ON `contacts_values`.`contact_id`=`contacts`.`id` '+
                'WHERE `clients`.`id` IN ('.concat(sql_query_tpl, ') ') +
                'ORDER BY `contacts_values`.`type` ASC';

            conn.query(query_sql, ids, (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }

                const responce = new Map();
                result.forEach((value) => {
                    if (!responce.has(value.contact_id)) {
                        responce.set(value.contact_id, [value.value]);
                    } else {
                        responce.get(value.contact_id).push(value.value);
                    }
                });

                resolve(responce);
            });
        }),
    ]).then((results) => {
        const clients = results[0]||[];
        const values = results[1];

        const VALUE_ON = '+';
        const VALUE_OFF = '-';

        clients.forEach((client, i) => {
            row = sheet.getRow(i+2);

            cell = row.getCell(1);
            cell.value = client.name;
   
            cell = row.getCell(2);
            cell.value = client.inn;
   
            cell = row.getCell(3);
            cell.value = client.city_name;

            cell = row.getCell(4);
            cell.value = client.address;
            // потребности
            const start_pos = 5;
            let compare_byte = 0;
            for (let pos = 0; pos < 11; pos++) {
                cell = row.getCell(start_pos + pos);
                
                compare_byte = 1 << pos;
                cell.value = (client.needs & compare_byte) === compare_byte ? VALUE_ON : VALUE_OFF;
            }

            // контактные лица
            cell = row.getCell(17);
            cell.value = client.contact_name;
            
            const list_contacts = values.get(client.contact_id);
            if (!list_contacts) {
                return;
            }
            
            list_contacts.forEach((contact, c) => {
                cell = row.getCell(18 + c);
                cell.value = contact;
            });
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", `attachment; filename=contacts.xlsx`);
        workbook.xlsx.write(res).then(() => {
            res.end();
        });
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500);
    });
});