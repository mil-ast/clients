
const conn = require('../db');

app.get('/api/regions', function (req, res) {
    const district_id = req.query.district_id|0;
    if (district_id === 0) {
        res.send(500);
        return;
    }

    const query_sql = 'SELECT * FROM `geo_regions` WHERE `district_id`=?';
    conn.query(query_sql, [district_id], (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        res.send(result);
    });
});
