const conn = require('../db');

app.get('/api/client/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'SELECT * FROM `clients` WHERE `id`=?';
    conn.query(query_sql, [id], (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        if (!Array.isArray(result) || result.length == 0) {
            res.send(404);
            return;
        }

        res.send(result[0]);
    });
});
