
const conn = require('../db');

app.get('/api/business_area', function (req, res) {
    const query_sql = 'SELECT * FROM `business_area`';
    conn.query(query_sql, (err, result) => {
        if (err !== null) {
            res.send(500);
            return;
        }

        res.send(result);
    });
});
