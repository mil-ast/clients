const conn = require('../db');

app.get('/api/client/:id/contacts', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const contacts_values = new Map();

    Promise.all([
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `id`,`name`,`comment`,`date_actual` FROM `contacts` WHERE `client_id`=?';
            conn.query(query_sql, [id], (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }
    
                resolve(result||[]);
            });
        }),
        new Promise((resolve, reject) => {
            const query_sql = 'SELECT `contacts`.`id` AS `contact_id`, `v`.`id`, `v`.`type`, `v`.`value` '+
                'FROM `contacts` '+
                'INNER JOIN `contacts_values` AS `v` ON `v`.`contact_id`=`contacts`.`id` '+
                'WHERE `contacts`.`client_id`=?';
            conn.query(query_sql, [id], (err, result) => {
                if (err !== null) {
                    reject(err);
                    return;
                }

                const arr = result||[];

                for (let i = 0; i < arr.length; i++) {
                    if (contacts_values.has(arr[i].contact_id)) {
                        contacts_values.get(arr[i].contact_id).push(arr[i]);
                    } else {
                        contacts_values.set(arr[i].contact_id, [arr[i]]);
                    }
                }
    
                resolve(null);
            });
        })
    ]).then((data) => {
        const contacts = data[0]||[];
        const responce = contacts.map((item) => {
            const contact = item;
            contact.values = contacts_values.get(item.id)||[];

            return contact;
        });

        res.send(responce);
    }).catch((err) => {
        console.log(err);
        res.send(500);
    });
});

/**
 * создание контакта
 */
app.post('/api/client/:id/contacts', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const date = new Date().toISOString().substr(0, 10);

    const query_sql = 'INSERT INTO `contacts` SET ?';
    const query_data = {
        client_id : id,
        name : req.body.name,
        comment : req.body.comment,
        date_actual : date,
    };

    conn.query(query_sql, query_data, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        const responce = {
            id : result.insertId,
            name : req.body.name,
            comment : req.body.comment,
            date_actual : date,
            values : []
        };

        res.status(201).send(responce);
    });
});

/**
 * изменение контакта
 */
app.put('/api/clients/contacts/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'UPDATE `contacts` SET `name`=?,`comment`=? WHERE `id`=?';

    const query_data = [
        req.body.name,
        req.body.comment,
        id
    ];

    conn.query(query_sql, query_data, (err) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        const responce = {
            id : id,
            name : req.body.name,
            comment : req.body.comment,
        };

        res.status(202).send(responce);
    });
});

/**
 * удалить контакт
 */
app.delete('/api/clients/contacts/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'DELETE FROM `contacts` WHERE `id`=?';

    conn.query(query_sql, [id], (err) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        res.send(204);
    });
});

/**
 * актуализировать дату
 */
app.put('/api/clients/contacts/update_date/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'UPDATE `contacts` SET `date_actual`=? WHERE `id`=?';

    const date = new Date().toISOString().substr(0, 10);
    conn.query(query_sql, [date, id], (err) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        res.send({
            id : id,
            date_actual : date
        });
    });
});

/**
 * добавить значение
 */
app.post('/api/clients/contacts/:id/contact', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'INSERT INTO `contacts_values` SET ?';

    const query_data = {
        contact_id : id,
        type : req.body.type,
        value : req.body.value,
    };

    conn.query(query_sql, query_data, (err, result) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        const responce = {
            id : result.insertId,
            type : req.body.type,
            value : req.body.value,
        };

        res.status(201).send(responce);
    });
});

/**
 * удалить значение
 */
app.delete('/api/clients/contacts/contact/:id', function (req, res) {
    const id = req.params.id|0;
    if (id === 0) {
        res.send(404);
        return
    }

    const query_sql = 'DELETE FROM `contacts_values` WHERE `id`=?';
    conn.query(query_sql, [id], (err, result) => {
        if (err !== null) {
            console.error(err);
            res.status(500);
            return;
        }

        res.send(204);
    });
});