const mysql = require('mysql');
const pool = mysql.createPool({
    host     : '127.0.0.1',
    port     : 3306,
    user     : 'root',
    password : '123321',
    database : 'contacts',
    connectTimeout : 5000,
    acquireTimeout : 5000,
    connectionLimit : 10,
    typeCast : (field, next) => {
        switch (field.type) {
            case 'LONGLONG': case 'DATE':
                return (field.string());
            default:
                return next();
        }
    },
    debug : false
});

module.exports = (() => {
    return {
        query: (query, params, callback) => {
            pool.query(query, params, (err, rows) => {
                if (!err) {
                    callback(null, rows);
                } else {
                    console.error(err);
                    callback(err, null);
                }
            });
        }
    };
})();