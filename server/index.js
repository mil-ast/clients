const express = require('express');
const app = express();
const http = require('http').Server(app);

const fs = require('fs');
const bodyParser = require('body-parser');

process.title = 'nodejs_clients';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

global.app = app;

app.use(express.static(__dirname.concat('/public')));

fs.readdirSync(__dirname.concat('/handlers')).forEach(function(fileName) {
    const name = __dirname.concat('/handlers/', fileName);
    const stat = fs.statSync(name);
    
    if (stat.isFile()) {
       require(name);
    }
});

app.on('error', (err) => {
    console.error(err);
});

http.listen(8001, function(){
    console.log('listening on *:8001');
});